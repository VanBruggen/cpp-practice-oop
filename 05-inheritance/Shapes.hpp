#ifndef JIPP_03_SHAPES_HPP
#define JIPP_03_SHAPES_HPP

#include <iostream>
#include <vector>
#include <limits>

void AbyKontynuowac();

// Zadanie 1:   ========================================================================================================

class Shape {
protected:
    std::string m_name;

public:
    explicit Shape(const std::string& name);
    virtual ~Shape();

    const std::string& getName() const {return m_name;}
    void setName(const std::string& newName) {m_name = newName;}

    virtual double area() = 0;
};

class Rectangle : public Shape {
protected:
    double m_width;
    double m_height;

public:
    Rectangle(const std::string& name, const double& width, const double& height);
    virtual ~Rectangle();

    double area();
};

class Square : public Rectangle {
public:
    Square(const std::string& name, const double& width);
    ~Square();

    // Bez sensu było dawać tutaj reimplementację metody area() (jak w diagramie), skoro robiłaby to samo i na tych samych
    // polach, co w klasie Rectangle. Wcześniej ją oczywiście dołączyłem, ale opierała się na zwróceniu Rectangle::area(),
    // więc doszedłem do wniosku, że lepiej, żeby klasa Square ją po prostu odziedziczyła.
};

// Zadanie 2:   ========================================================================================================

class Circle {
protected:
    double m_radius;

public:
    explicit Circle(const double& radius);
    virtual ~Circle();

    const double& getRadius() const {return m_radius;}

    double area();
};

class Cylinder : public Circle {
protected:
    double m_height;

public:
    Cylinder (const double& radius, const double& height);
    ~Cylinder();

    double area();
    double volume();
};

#endif //JIPP_03_SHAPES_HPP
