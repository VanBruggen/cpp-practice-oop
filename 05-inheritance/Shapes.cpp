#include "Shapes.hpp"

void AbyKontynuowac() {

    std::cout << "\n\nAby kontynuowac, wprowadz \" \' \" (apostrof).\n\n";

    char c;

    do {
        std::cout << "> ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        c = char(std::cin.get());
    } while (c != '\'');
}

// Zadanie 1:   ========================================================================================================

Shape::Shape(const std::string& name) :
    m_name(name) {

        std::cout << "\n\nWywolano konstruktor klasy Shape.";
        std::cout << "\nNazwa ksztaltu: " << getName();
}

Shape::~Shape() {

        std::cout << "\n\nWywolano destruktor klasy Shape.";
        std::cout << "\nNazwa ksztaltu: " << getName();
}

Rectangle::Rectangle(const std::string& name, const double& width, const double& height) :
    Shape(name),
    m_width(width),
    m_height(height) {

        std::cout << "\n\nWywolano konstruktor klasy Rectangle.";
        std::cout << "\nNazwa prostokata: " << getName();
}

Rectangle::~Rectangle() {

        std::cout << "\n\nWywolano destruktor klasy Rectangle.";
        std::cout << "\nNazwa prostokata: " << getName();
}

double Rectangle::area() {

    return m_height*m_width;
}

Square::Square(const std::string& name, const double& width) :
    Rectangle(name, width, width) {

        std::cout << "\n\nWywolano konstruktor klasy Square.";
        std::cout << "\nNazwa kwadratu: " << getName();
}

Square::~Square() {

        std::cout << "\n\nWywolano destruktor klasy Square.";
        std::cout << "\nNazwa kwadratu: " << getName();
}

// Zadanie 2:   ========================================================================================================

Circle::Circle(const double &radius) :
    m_radius(radius) {
        std::cout << "\n\nWywolano konstruktor klasy Circle.";
}

Circle::~Circle() {
        std::cout << "\n\nWywolano destruktor klasy Circle.";
}

double Circle::area() {
    return 3.1415 * m_radius * m_radius;
}

Cylinder::Cylinder(const double &radius, const double &height) :
    Circle(radius),
    m_height(height) {
        std::cout << "\n\nWywolano konstruktor klasy Cylinder.";
}

Cylinder::~Cylinder() {
        std::cout << "\n\nWywolano destruktor klasy Cylinder.";
}

double Cylinder::area() {
    return 2 * Circle::area() + (2 * 3.1415 * m_radius * m_height);
}

double Cylinder::volume() {
    return Circle::area() * m_height;
}