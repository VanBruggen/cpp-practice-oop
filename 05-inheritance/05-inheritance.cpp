#include "Shapes.hpp"

// Zadanie 1.   ========================================================================================================
void zadanie_01() {

    //Shape sh01("Shape01");    Nie zadziała, ze względu na to, że klasa Shape posiada metodę czysto wirtualną, co czyni
    //tę klasę tzw. klasą abstrakcyjną.

    Rectangle rec01("Rectangle01", 4, 3);
    Square sq01("Square01", 4);

    std::cout << "\n\n(niezarzutowany) Pole prostokata o bokach 3 i 4: " << rec01.area();
    std::cout << "\n(niezarzutowany) Pole kwadratu o boku 4: " << sq01.area();

    Shape& rec01_sh_ref = rec01;
    Shape& sq01_sh_ref = sq01;
    Rectangle& sq01_rec_ref = sq01;

    std::cout << "\n\n(referencja na Shape) Pole prostokata o bokach 3 i 4: " << rec01_sh_ref.area();
    std::cout << "\n(referencja na Shape) Pole kwadratu o boku 4: " << sq01_sh_ref.area();
    std::cout << "\n(referencja na Rectangle) Pole kwadratu o boku 4: " << sq01_rec_ref.area();

    Shape* rec01_sh_ptr = &rec01;
    Shape* sq01_sh_ptr = &sq01;
    Rectangle* sq01_rec_ptr = &sq01;

    std::cout << "\n\n(wskaznik na Shape) Pole prostokata o bokach 3 i 4: " << rec01_sh_ptr-> area();
    std::cout << "\n(wskaznik na Shape) Pole kwadratu o boku 4: " << sq01_sh_ptr-> area();
    std::cout << "\n(wskaznik na Rectangle) Pole kwadratu o boku 4: " << sq01_rec_ptr-> area();
}

// Nie zauważyłem żadnej różnicy pomiędzy sytuacją, kiedy metody area() są wirtualne, a kiedy nie są.
// Pewnie dlatego, że w implementacjach w klasach potomnych zabrakło słów kluczowych "override".

// Przy konstrukcji obiektu klasy potomnej, najpierw wywoływane są konstruktory klas wyższych.
// Przy destrukcji jest dokładnie na odwrót.

// Po ustawieniu trybu dziedziczenia klasy Square na protected, wystąpił błąd kompilacji, wynikający z tego,
// że w przypadku dziedziczenia w trybie protected, nie można rzutować obiektów klasy dziedziczącej na klasy wyższe.

// Zadanie 2.   ========================================================================================================
void zadanie_02() {

    Circle c(4);
    std::cout << "\n\nKolo o promienu: 4";
    std::cout << "\nPole: " << c.area();

    Cylinder cyl(3, 5);
    std::cout << "\n\nWalec o promieniu podstawy 3 i wysokosci 5";
    std::cout << "\nPole powierzchni: " << cyl.area();
    std::cout << "\nObjetosc: " << cyl.volume();

    Circle& cyl_c_ref = cyl;
    std::cout << "\n\nTen sam walec zarzutowany referencja na kolo";
    std::cout << "\nPromien: " << cyl_c_ref.getRadius();
    std::cout << "\nPole: " << cyl_c_ref.area();        // Kiedy w klasie Circle (nadrzędnej) ustawiam metodę area() jako
                                                        // wirtualną, program korzysta z implementacji klasy podrzędnej.
                                                        // Gdyby typ referencji / wskaźnika był zgodny z typem instancji,
                                                        // z którą dana referencja / wskaźnik jest związany, to pomimo
                                                        // oznaczenia metody jako wirtualnej, program wykorzystałby jej
                                                        // definicję. Na tym chyba polega metoda częściowo wirtualna.

                                                        // Wniosek: Oznaczanie metod jako wirtualne ma sens tylko
                                                        // w klasach nadrzędnych. Dotyczy to również destruktorów, o czym
                                                        // przekonałem się poniżej.
    std::cout << "\nObjetosc: brak dostepu.";

    Cylinder* cyl_2_ptr = new Cylinder(2, 3);
    Circle* cyl_2_c_ptr = cyl_2_ptr;
    delete cyl_2_c_ptr;     // Wywołał się również destruktor klasy Cylinder.
                            // Wniosek: jako wirtualny musi byc oznaczony wyłącznie destruktor klasy nadrzędnej.
                            // Uwaga: Clang-Tidy sugeruje, żeby dopisać za destruktorem klasy Cylinder "override" albo (rzadko) "final".

    AbyKontynuowac();
}

// Blok główny programu     ============================================================================================
int main() {

    bool stop_menu_01 = false;

    do {

        std::cout << "\n\n====================================================================================="
                     "\n\nWitaj w podgladzie zadan z 3. laboratorium JiPP! Wybierz zadanie, ktorego dzialanie\n"
                     "chcesz sprawdzic albo zakoncz program:\n\n";
        std::cout << "\"1\" - zadanie 1.\n";
        std::cout << "\"2\" - zadanie 2.\n";
        std::cout << "\"x\" - zakoncz program\n\n";

        std::cout << "> ";

        char choice;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.get(choice);

        switch (choice) {

            case '1': {
                std::cout << "\n\n================="
                             "\n\nZadanie 1:";
                zadanie_01();
                AbyKontynuowac();
                break;
            }

            case '2': {
                std::cout << "\n\n================="
                             "\n\nZadanie 2:";
                zadanie_02();
                AbyKontynuowac();
                break;
            }

            case 'x': {
                std::cout << "\n\nKoncze dzialanie programu...";
                stop_menu_01 = true;
                break;
            }

            default: {
                std::cout << "\n\nNie wybrano zadnej opcji. Restartuje program...";
                AbyKontynuowac();
            }
        }

    } while (!stop_menu_01);

    return 0;
}