#include <iostream>
#include <cmath>
#include <exception>

short amount = 0;

class no_real_solution : public std::runtime_error {
public:
    no_real_solution() :
        std::runtime_error{"\n\nBrak rozwiazan w dziedzinie liczb rzeczywistych."} {}
};

struct BinomialSolver {

    short ID;

    float a, b, c;
    float x1, x2;
    float delta;

    BinomialSolver (float a, float b, float c) {
        this -> a = a;
        this -> b = b;
        this -> c = c;

        ++amount;
        ID = amount;

        std::cout << "Trojmian kwadratowy " << ID << ":"
                  << "\n\t(" << this -> a << ")x^2 + (" << this -> b << ")x + (" << this -> c << ")\n\n";

        delta = b*b - 4*a*c;

        if (delta < 0.0) throw no_real_solution{};

        if (delta > 0.0) {
            this -> x1 = (-b + sqrtf(delta)) / 2*a;
            this -> x2 = (-b - sqrtf(delta)) / 2*a;

        } else if (delta == 0.0) {
            this -> x1 = (-b) / 2 * a;
            this -> x2 = this -> x1;
        }
    }

    ~BinomialSolver() {

        std::cout << "Trojmian kwadratowy " << ID << " zostal usuniety.\n\n";
        --amount;
    }

    float getA() const {return a;}
    float getB() const {return b;}
    float getC() const {return c;}

    float getX1() const {
        if (delta < 0.0) {
            std::cout << "Trojmian kwadratowy " << ID << " nie ma pierwiastkow. Zwracam wartosc \"-1\".\n\n";
            return float(-1.0);

        } else return x1;
    }

    float getX2() const {
        if (delta < 0.0) {
            std::cout << "Trojmian kwadratowy " << ID << " nie ma pierwiastkow. Zwracam wartosc \"-1\".\n\n";
            return float(-1.0);

        } else return x2;
    }

    float calculate (float arg) const {

        return (a*arg*arg + b*arg + c);
    }
};

int main() {

    try {

        BinomialSolver trojmian_01 (6, 3, 9);
        BinomialSolver trojmian_02 (5.4, float(-1.5), float(-3.0));

        std::cout << "Proba dostepu do parametru \"a\" trojmianu kwadratowego " << trojmian_01.ID << ": " << trojmian_01.getA() << "\n\n";
        std::cout << "Proba dostepu do parametru \"b\" trojmianu kwadratowego " << trojmian_01.ID << ": " << trojmian_01.getB() << "\n\n";
        std::cout << "Proba dostepu do parametru \"c\" trojmianu kwadratowego " << trojmian_01.ID << ": " << trojmian_01.getC() << "\n\n";

        std::cout << "Pierwiastki trojmianu kwadratowego " << trojmian_01.ID << ": \n"
                  << "\tx1: " << trojmian_01.getX1() << "\n"
                  << "\tx2: " << trojmian_01.getX2() << "\n\n";

        std::cout << "Proba dostepu do parametru \"a\" trojmianu kwadratowego " << trojmian_02.ID << ": " << trojmian_02.getA() << "\n\n";
        std::cout << "Proba dostepu do parametru \"b\" trojmianu kwadratowego " << trojmian_02.ID << ": " << trojmian_02.getB() << "\n\n";
        std::cout << "Proba dostepu do parametru \"c\" trojmianu kwadratowego " << trojmian_02.ID << ": " << trojmian_02.getC() << "\n\n";

        std::cout << "Pierwiastki trojmianu kwadratowego " << trojmian_02.ID << ": \n"
                  << "\tx1: " << trojmian_02.getX1() << "\n"
                  << "\tx2: " << trojmian_02.getX2() << "\n\n";

        std::cout << "Wartosc trojmianu kwadratowego " << trojmian_01.ID << " dla argumentu \"" << 3.2 << "\": " << trojmian_01.calculate(3.2) << "\n\n";
        std::cout << "Wartosc trojmianu kwadratowego " << trojmian_01.ID << " dla argumentu \"" << -2.1 << "\": " << trojmian_01.calculate(-2.1) << "\n\n";

        std::cout << "Wartosc trojmianu kwadratowego " << trojmian_02.ID << " dla argumentu \"" << 3.2 << "\": " << trojmian_02.calculate(3.2) << "\n\n";
        std::cout << "Wartosc trojmianu kwadratowego " << trojmian_02.ID << " dla argumentu \"" << -2.1 << "\": " << trojmian_02.calculate(float(-2.1)) << "\n\n";

    } catch (const no_real_solution& nrs) {

        std::cout << nrs.what();
    }

    return 0;
}