cmake_minimum_required(VERSION 3.23)
project(02-trycatch01-getter-setter-method)

set(CMAKE_CXX_STANDARD 20)

add_executable(02-trycatch01-getter-setter-method 02-trycatch01-getter-setter-method.cpp)