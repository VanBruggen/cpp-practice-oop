#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

struct Complex {

    float a;
    float b;

    Complex (float a, float b) {        // ten konstruktor nie był potrzebny, ale stworzyłem go dla zasady :)

        this -> a = a;
        this -> b = b;
    }

    Complex() {

        this -> a = 0.0;
        this -> b = 0.0;
    }

    float modulus() {

        return sqrtf( (a*a)+(b*b) );
    }
};

void displayComplex(Complex& cplx) {

    std::cout << cplx.a << " + (" << cplx.b << ")i,\tmodul: " << cplx.modulus() << "\n";
}

Complex loadComplex(std::fstream & file) {

    Complex tempComplex{};

    file >> tempComplex.a;
    file >> tempComplex.b;

    return tempComplex;
}

int main() {

    std::fstream source("source.txt", std::ios::in);
    std::vector<Complex> v;

    if ( source.is_open() && source.good() ) {

        while ( !source.eof() ) {

            v.push_back(loadComplex(source));
        }

        Complex biggestModulus{};

        std::cout << "\n";

        for (Complex &cplx : v) {

            if ( cplx.modulus() > biggestModulus.modulus() ) {biggestModulus = cplx;}
            displayComplex(cplx);
        }

        std::cout << "\nLiczba o najwiekszym module:\n";
        displayComplex(biggestModulus);
    }

    source.close();
    return 0;
}