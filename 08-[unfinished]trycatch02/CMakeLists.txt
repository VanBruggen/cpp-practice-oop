cmake_minimum_required(VERSION 3.23)
project(trycatch02)

set(CMAKE_CXX_STANDARD 20)

add_executable(trycatch02 trycatch02.cpp Vector.hpp)