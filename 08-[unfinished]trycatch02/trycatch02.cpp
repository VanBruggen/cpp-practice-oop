#include "Vector.hpp"
#include <limits>
#include <cstdlib>
#include <ctime>

void AbyKontynuowac() {

    std::cout << "\n\nAby kontynuowac, wprowadz \" \' \" (apostrof).\n\n";

    char c;

    do {
        std::cout << "> ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        c = char(std::cin.get());
    } while (c != '\'');
}

void zadanie_01() {

    srand(time(nullptr));

    try {

        int n;

        std::cout << "\n\nPodaj rozmiar wektora V1: ";
        std::cin >> n;

        Vector<int> v1(n);

        for (size_t i = 0; i < n; i++)
            v1[i] = rand() % 100;

        std::cout << "\n\nV1: " << v1 << std::endl << std::endl;
        std::cout << "\n\nKtory element V1 chcesz wyswietlic? (od 0 do " << (n-1) << ")";
        std::cin >> n;
        std::cout << "\n\n" << n << "-ty element V1 to: " << v1[n] << std::endl << std::endl;

        std::cout << "\n\nPodaj rozmiar wektora V2: ";
        std::cin >> n;

        Vector<int> v2(n);

        for (size_t i = 0; i < n; i++)
            v2[i] = rand() % 100;

        std::cout << "\n\nV2: " << v2 << std::endl << std::endl;
        //std::cout << "\nV1 + V2: " << (v1 + v2) << std::endl;
        std::cout << "\nV1 - V2: " << (v1 - v2) << std::endl;

    } catch (const std::domain_error& de) {

        std::cout << "\n\n" << de.what();

    } catch (const std::out_of_range& oor) {

        std::cout << "\n\n" << oor.what();
    }

    AbyKontynuowac();
}

void zadanie_02() {


}

int main() {

    bool stop_menu_01 = false;

    do {

        std::cout << "\n\n====================================================================================="
                     "\n\nWitaj w podgladzie zadan z 6. laboratorium JiPP! Wybierz zadanie, ktorego dzialanie\n"
                     "chcesz sprawdzic albo zakoncz program:\n\n";
        std::cout << "\"1\" - zadanie 1.\n";
        std::cout << "\"2\" - zadanie 2.\n";
        //std::cout << "\"3\" - zadanie 3.\n";
        std::cout << "\"x\" - zakoncz program\n\n";

        std::cout << "> ";

        char choice;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.get(choice);

        switch (choice) {

            case '1': {
                std::cout << "\n\n================="
                             "\n\nZadanie 1:\n";
                zadanie_01();
                break;
            }

            case '2': {
                std::cout << "\n\n================="
                             "\n\nZadanie 2:\n";
                zadanie_02();
                break;
            }

                /*case '3': {
                    std::cout << "\n\n================="
                                 "\n\nZadanie 3:\n";
                    zadanie_03();
                    break;
                }*/

            case 'x': {
                std::cout << "\n\nKoncze dzialanie programu...\n\n";
                stop_menu_01 = true;
                break;
            }

            default: {
                std::cout << "\n\nNie wybrano zadnej opcji. Restartuje program...\n";
                AbyKontynuowac();
            }
        }

    } while (!stop_menu_01);

    return 0;
}