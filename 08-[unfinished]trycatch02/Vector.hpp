#ifndef JIPP_06_NAUKA_VECTOR_HPP
#define JIPP_06_NAUKA_VECTOR_HPP

#include <cstdlib>
#include <iostream>
#include <exception>

template <typename T>
class Vector {
    size_t m_dimensions;
    T* m_data;

public:
    explicit Vector<T>(const size_t &dimensions) :
            m_dimensions(dimensions), m_data(new T[dimensions]) {

        if (dimensions == 0) {

            delete[] m_data;

            throw std::domain_error{"Wektor musi miec co najmniej jeden wymiar!"};
        }

        std::cout << "\n\nWywolano konstruktor glowny.";
    }


    Vector<T> (const Vector<T>& otherVector) :
            m_dimensions(otherVector.dimensions()),
            m_data(new T[otherVector.dimensions()]) {

        for (size_t i=0; i < m_dimensions; ++i) {

            m_data[i] = otherVector[i];
        }

        std::cout << "\n\nWywolano konstruktor kopiujacy.";
    }

    virtual ~Vector<T>() {

        if (m_data) delete[] m_data;
    }

    Vector<T>& operator = (const Vector<T>& otherVector) {

        m_dimensions = otherVector.dimensions();

        delete[] m_data;
        m_data = new T[m_dimensions];

        for (size_t i=0; i < m_dimensions; ++i) {

            m_data[i] = otherVector[i];
        }

        std::cout << "\n\nWywolano operator przypisania.";

        return *this;
    }

    T& operator [] (const size_t& index) {

        if (index > dimensions() || index < 0) throw std::out_of_range{"Podano indeks spoza zakresu wektora!"};

        return m_data[index];
    }

    const T& operator [] (const size_t& index) const {

        if (index > dimensions() || index < 0) throw std::out_of_range{"Podano indeks spoza zakresu wektora!"};

        return m_data[index];
    }

    const size_t& dimensions() const {

        return m_dimensions;
    }

    Vector<T> operator + (const Vector<T>& otherVector) const {

        if (this-> dimensions() != otherVector.dimensions()) throw std::out_of_range{"Dodawane wektory musza miec "
                                                                                     "taka sama liczbe wymiarow!"};

        Vector<T> resultVector(this-> dimensions());

        for (size_t i=0; i < resultVector.dimensions(); ++i) {

            resultVector[i] = this->m_data[i] + otherVector[i];
        }

        return resultVector;
    }

    Vector<T> operator - (const Vector<T>& otherVector) const {

        if (this-> dimensions() != otherVector.dimensions()) throw std::out_of_range{"Odejmowane wektory musza miec "
                                                                                     "taka sama liczbe wymiarow!"};

        Vector<T> resultVector(this->dimensions() >= otherVector.dimensions() ? this->dimensions() : otherVector.dimensions());

        for (size_t i=0; i < resultVector.dimensions(); ++i) {

            resultVector[i] = this->m_data[i] - otherVector[i];
        }

        return resultVector;
    }

    bool operator == (const Vector<T>& otherVector) {

        if (this == &otherVector) return true;

        if (this->dimensions() != otherVector.dimensions()) return false;

        for (size_t i = 0; i < this->dimensions(); ++i) {

            if (this->m_data[i] != otherVector[i]) return false;
        }

        return true;
    }

    bool operator != (const Vector<T>& otherVector) {

        if (this == &otherVector) return false;

        if (this->dimensions() != otherVector.dimensions()) return true;

        for (size_t i = 0; i < this->dimensions(); ++i) {

            if (this->m_data[i] != otherVector[i]) return true;
        }

        return false;
    }

    //friend std::ostream& operator << (std::ostream& stream, const Vector<T>& vector);
};

template <typename T>
std::ostream& operator << (std::ostream& stream, const Vector<T>& vector) {

    for (size_t i=0; i < vector.dimensions(); ++i) {

        stream << vector[i] << "\t";
    }

    stream << "\n";

    return stream;
}

#endif //JIPP_06_NAUKA_VECTOR_HPP
