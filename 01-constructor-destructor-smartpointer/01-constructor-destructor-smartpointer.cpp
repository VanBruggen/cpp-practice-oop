#include <iostream>
#include <memory>

struct VerboseObject {

    std::string name;

    void sayHello() {

        std::cout << "\n\t\t> Structure \"" << this -> name << "\" sends its regards.";
    }

    explicit VerboseObject(std::string name) {

        this -> name = name;
        std::cout << "\n\t\t> Structure \"" << this -> name << "\" created.";
    }

    ~VerboseObject() {

        std::cout << "\n\t\t> Structure \"" << this -> name << "\" destroyed.";
    }
};

void automaticStaticStruct() {

    VerboseObject s1 = VerboseObject{"Struct_01"};
    s1.sayHello();
}

void automaticDynamicStruct() {

    std::unique_ptr<VerboseObject> up_s2 = std::make_unique<VerboseObject>("Struct_02");
    up_s2->sayHello();
}

void manualDynamicStruct() {

    VerboseObject* p_s3 = new VerboseObject{"Struct_03"};
    p_s3->sayHello();
    delete p_s3;
}

int main() {

    std::cout << "\n\nOrder of invoking constructors and destructors in:"
                 "\n\n\ta) Automatically created static structure:\n";
    automaticStaticStruct();

    std::cout << "\n\n\tb) Automatically created dynamic structure:\n";
    automaticDynamicStruct();

    std::cout << "\n\n\tc) Manually created dynamic structure:\n";
    manualDynamicStruct();

    std::cout << "\n";

    return 0;
}

/* Wnioski:
 * 1) Jedyna różnica polega na tym, że strukturę 3. należy usunąć ręcznie poprzez polecenie "delete".
 */