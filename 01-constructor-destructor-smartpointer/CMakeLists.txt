cmake_minimum_required(VERSION 3.23)
project(01-constructor-destructor-smartpointer)

set(CMAKE_CXX_STANDARD 20)

add_executable(01-constructor-destructor-smartpointer 01-constructor-destructor-smartpointer.cpp)