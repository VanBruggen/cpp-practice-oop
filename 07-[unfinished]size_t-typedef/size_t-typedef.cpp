#include "Vector.hpp"
#include "Shapes.hpp"

void zadanie_01() {

    typedef int T;

    const size_t d = 10;

    Vector<T> v1(d);

    for (size_t i = 0; i < d; ++i) {

        v1[i] = (T) i;
    }

    std::cout << "\n\nWektor v1: " << v1 << std::endl;

    Vector<T> v2 = v1 + v1;

    std::cout << "\n\nWektor v2: " << v2 << std::endl;
    std::cout << "\nv1 == v1: " << (v1 == v1 ? "TAK" : "NIE") << std::endl;
    std::cout << "\nv1 != v1: " << (v1 != v1 ? "TAK" : "NIE") << std::endl;
    std::cout << "\nv1 == v2: " << (v1 == v2 ? "TAK" : "NIE") << std::endl;
    std::cout << "\nv1 != v2: " << (v1 != v2 ? "TAK" : "NIE") << std::endl;

    Vector<T> v3(5);

    std::cout << "\n\nWektor v3: " << v3 << std::endl;
    std::cout << "\nv1 == v3: " << (v1 == v3 ? "TAK" : "NIE") << std::endl;

    v3 = v1;

    std::cout << "\n\nWektor v3: " << v3 << std::endl;
    std::cout << "\nv1 == v3: " << (v1 == v3 ? "TAK" : "NIE") << std::endl;

    AbyKontynuowac();
}

void zadanie_02() {

    ShapeContainer sp;

    Shape* rec01 = new Rectangle("Rectangle_01", 3, 4);
    Shape* sq01 = new Square("Square_01", 3);
    Shape* cir01 = new Circle("Circle_01", 5);
    Shape* elipse01 = new Elipse("Elipse_01", 4, 3);

    sp.add(rec01);
    sp.add(sq01);
    sp.add(cir01);
    sp.add(elipse01);

    sp.displayAll();

    AbyKontynuowac();

    sp << new Rectangle("Rectangle_02", 5, 6);

    sp.displayAll();

    AbyKontynuowac();
}

int main() {

    bool stop_menu_01 = false;

    do {

        std::cout << "\n\n====================================================================================="
                     "\n\nWitaj w podgladzie zadan z 5. laboratorium JiPP! Wybierz zadanie, ktorego dzialanie\n"
                     "chcesz sprawdzic albo zakoncz program:\n\n";
        std::cout << "\"1\" - zadanie 1.\n";
        std::cout << "\"2\" - zadanie 2.\n";
        std::cout << "\"x\" - zakoncz program\n\n";

        std::cout << "> ";

        char choice;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.get(choice);

        switch (choice) {

            case '1': {
                std::cout << "\n\n================="
                             "\n\nZadanie 1:";
                zadanie_01();
                AbyKontynuowac();
                break;
            }

            case '2': {
                std::cout << "\n\n================="
                             "\n\nZadanie 2:";
                zadanie_02();
                AbyKontynuowac();
                break;
            }

            case 'x': {
                std::cout << "\n\nKoncze dzialanie programu...";
                stop_menu_01 = true;
                break;
            }

            default: {
                std::cout << "\n\nNie wybrano zadnej opcji. Restartuje program...";
                AbyKontynuowac();
            }
        }

    } while (!stop_menu_01);

    return 0;
}