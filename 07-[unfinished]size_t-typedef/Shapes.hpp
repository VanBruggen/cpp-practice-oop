#ifndef JIPP_04_SHAPES_HPP
#define JIPP_04_SHAPES_HPP

#include <iostream>
#include <vector>
#include <limits>
#include <cmath>

void AbyKontynuowac();

// ======================================================================================================== Zadanie 1:

//  ======================================================================================   Shape
class Shape {
    std::string m_name;

public:
    explicit Shape(std::string name);
    virtual ~Shape();

    const std::string& getName() const {return m_name;}
    void setName(const std::string& newName) {m_name = newName;}

    virtual double area() const = 0;
    virtual double perimeter() const = 0;
};

//  =====================================================================================   Rectangle
class Rectangle : public Shape {
    double m_width;
    double m_height;

public:
    Rectangle(std::string name, const double& width, const double& height);
    ~Rectangle() override;

    double area() const override;
    double perimeter() const override;
};

//  ======================================================================================   Square
class Square : public Rectangle {
public:
    Square(std::string name, const double& width);
    ~Square() override;
};

//  ======================================================================================   Circle
class Circle : public Shape {
protected:
    double m_radius;

public:
    Circle(std::string name, const double& radius);
    ~Circle() override;

    const double& getRadius() const {return m_radius;}

    double area() const override;
    double perimeter() const override;
};

//  =======================================================================================   Elipse
class Elipse : public Shape {
protected:
    double m_majorAxis;
    double m_minorAxis;

public:
    Elipse(std::string name, const double& majorAxis, const double& minorAxis);
    ~Elipse() override;

    double area() const override;
    double perimeter() const override;
};

//  =======================================================================================   ShapeContainer
class ShapeContainer {
    std::vector<Shape*> shapeList;

public:
    ~ShapeContainer();

    void add(Shape* pShape);
    void displayAll() const;
    double totalArea() const;
    std::vector<Shape*> getGreaterThan(const double& area);
    ShapeContainer& operator << (Shape* pShape);
};

#endif //JIPP_04_SHAPES_HPP
