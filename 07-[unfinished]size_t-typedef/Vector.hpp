#ifndef JIPP_05_NAUKA_VECTOR_HPP
#define JIPP_05_NAUKA_VECTOR_HPP

#include <cstdlib>
#include <iostream>

template <typename T>
class Vector {
    size_t m_size;
    T* m_data;

public:
    explicit Vector<T>(const size_t& size) :
        m_size(size), m_data(new T[size]) {

        std::cout << "\n\nWywolano konstruktor glowny.";
    }

    Vector<T> (const Vector<T>& otherVector) :
            m_size(otherVector.size()),
            m_data(new T[otherVector.size()]) {

        for (size_t i=0; i < m_size; ++i) {

            m_data[i] = otherVector[i];
        }

        std::cout << "\n\nWywolano konstruktor kopiujacy.";
    }

    virtual ~Vector<T>() {

        if (m_data) delete[] m_data;
    }

    Vector<T>& operator = (const Vector<T>& otherVector) {

        m_size = otherVector.size();

        delete[] m_data;
        m_data = new T[m_size];

        for (size_t i=0; i < m_size; ++i) {

            m_data[i] = otherVector[i];
        }

        std::cout << "\n\nWywolano operator przypisania.";

        return *this;
    }

    T& operator [] (const size_t& index) {

        return m_data[index];
    }

    const T& operator [] (const size_t& index) const {

        return m_data[index];
    }

    const size_t& size() const {

        return m_size;
    }

    Vector<T> operator + (const Vector<T>& otherVector) const {

        Vector<T> resultVector(this->size() >= otherVector.size() ? this->size() : otherVector.size());

        for (size_t i=0; i < resultVector.size(); ++i) {

            resultVector[i] = this->m_data[i] + otherVector[i];
        }

        return resultVector;
    }

    Vector<T> operator - (const Vector<T>& otherVector) const {

        Vector<T> resultVector(this->size() >= otherVector.size() ? this->size() : otherVector.size());

        for (size_t i=0; i < resultVector.size(); ++i) {

            resultVector[i] = this->m_data[i] - otherVector[i];
        }

        return resultVector;
    }

    bool operator == (const Vector<T>& otherVector) {

        if (this == &otherVector) return true;

        if (this->size() != otherVector.size()) return false;

        for (size_t i = 0; i < this->size(); ++i) {

            if (this->m_data[i] != otherVector[i]) return false;
        }

        return true;
    }

    bool operator != (const Vector<T>& otherVector) {

        if (this == &otherVector) return false;

        if (this->size() != otherVector.size()) return true;

        for (size_t i = 0; i < this->size(); ++i) {

            if (this->m_data[i] != otherVector[i]) return true;
        }

        return false;
    }

    //friend std::ostream& operator << (std::ostream& stream, const Vector<T>& vector);
};

template <typename T>
std::ostream& operator << (std::ostream& stream, const Vector<T>& vector) {

    for (size_t i=0; i < vector.size(); ++i) {

        stream << vector[i] << "\t";
    }

    stream << "\n";

    return stream;
}

#endif //JIPP_05_NAUKA_VECTOR_HPP