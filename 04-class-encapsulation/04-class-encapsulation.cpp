#include "TimeSpan.hpp"
#include "StudentDatabase.hpp"
#include <memory>

// Zadanie 1.   ========================================================================================================
void zadanie_01() {

    std::unique_ptr<TimeSpan> ts = std::make_unique<TimeSpan>(3964);
    ts-> print();

    std::unique_ptr<TimeSpan> ts2 = std::make_unique<TimeSpan>();
    TimeSpan ts3{};
    TimeSpan ts4(5);
    TimeSpan ts5;

    ts2-> print();
    ts3.print();
    ts4.print();
    ts5.print();

    std::cout << "\n\n" << ts-> getHours() << ", " << ts-> getMinutes() << ", " << ts-> getSeconds() << ", " << ts4.getSeconds();

    AbyKontynuowac();
}

// Zadanie 2.   ========================================================================================================
void zadanie_02() {

    bool stop_menu_02 = false;

    StudentDatabase sd;

    do{

        std::cout << "\n\nWitaj w interfejsie bazy danych osobowych studentow! Dostepne operacje:"
                     "\n\n\t\"a\" - dodaj nowego studenta."
                     "\n\t\"p\" - wyswietl zawartosc bazy."
                     "\n\t\"f\" - znajdz studenta po numerze indeksu."
                     "\n\t\"r\" - usun studenta o podanym numerze indeksu."
                     "\n\t\"s\" - zapisz zawartosc bazy do pliku."
                     "\n\t\"l\" - zaladuj zawartosc bazy z pliku."
                     "\n\t\"c\" - wyczysc baze (wymaga potwierdzenia)."
                     "\n\t\"x\" - wroc do wyboru zadania.\n\n"
                     "> ";

        char choice;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin >> choice;

        switch (choice) {

            case 'a': sd.addStudent(); break;
            case 'p': sd.print(); break;
            case 'f': sd.find(); break;
            case 'r': sd.removeStudent(); break;
            case 's': sd.save(); break;
            case 'l': sd.load(); break;
            case 'c': sd.clear(); break;

            case 'x':
                std::cout << "\n\nWychodze z interfejsu...";
                stop_menu_02 = true;
                break;

            default:
                std::cout << "\n\nNie wybrano zadnej operacji. Restartuje interfejs...";
                AbyKontynuowac();
        }

    } while (!stop_menu_02);
}

// Zadanie 3.   ========================================================================================================
/*void zadanie_03() {

    std::cout << "\nJeszcze niezrobione.\n\n";

    AbyKontynuowac();
}*/

// Blok główny programu     ============================================================================================
int main() {

    bool stop_menu_01 = false;

    do {

        std::cout << "\n\n====================================================================================="
                     "\n\nWitaj w podgladzie zadan z 2. laboratorium JiPP! Wybierz zadanie, ktorego dzialanie\n"
                     "chcesz sprawdzic albo zakoncz program:\n\n";
        std::cout << "\"1\" - zadanie 1.\n";
        std::cout << "\"2\" - zadanie 2.\n";
        //std::cout << "\"3\" - zadanie 3.\n";
        std::cout << "\"x\" - zakoncz program\n\n";

        std::cout << "> ";

        char choice;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cin.get(choice);

        switch (choice) {

            case '1': {
                std::cout << "\n\n================="
                             "\n\nZadanie 1:\n";
                zadanie_01();
                break;
            }

            case '2': {
                std::cout << "\n\n================="
                             "\n\nZadanie 2:\n";
                zadanie_02();
                break;
            }

            /*case '3': {
                std::cout << "\n\n================="
                             "\n\nZadanie 3:\n";
                zadanie_03();
                break;
            }*/

            case 'x': {
                std::cout << "\n\nKoncze dzialanie programu...\n\n";
                stop_menu_01 = true;
                break;
            }

            default: {
                std::cout << "\n\nNie wybrano zadnej opcji. Restartuje program...\n";
                AbyKontynuowac();
            }
        }

    } while (!stop_menu_01);

    return 0;
}