#ifndef JIPP_02_TIMESPAN_HPP
#define JIPP_02_TIMESPAN_HPP

#include <iostream>

class TimeSpan {
    unsigned int m_hours;
    unsigned int m_minutes;     // [0 .. 59]
    unsigned int m_seconds;     // [0 .. 59]

public:
    TimeSpan();
    explicit TimeSpan(unsigned int seconds);
    const unsigned int& getHours() const {return m_hours;}
    const unsigned int& getMinutes() const {return m_minutes;}
    const unsigned int& getSeconds() const {return m_seconds;}
    void print();
};

#endif //JIPP_02_TIMESPAN_HPP
