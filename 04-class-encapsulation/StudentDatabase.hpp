#ifndef JIPP_02_STUDENTDATABASE_HPP
#define JIPP_02_STUDENTDATABASE_HPP

#include <iostream>
#include <vector>
#include <fstream>
#include <limits>

void AbyKontynuowac();

class Student {
    unsigned int m_ID;
    std::string m_name;
    std::string m_lastName;

public:
    // gettery i settery
    const unsigned int getID() const {return m_ID;}
    void setID(const unsigned int &newID) {m_ID = newID;}

    const std::string &getName() const {return m_name;}
    void setName(const std::string &newName) {m_name = newName;}

    const std::string &getLastName() const {return m_lastName;}
    void setLastName(const std::string &newLastName) {m_lastName = newLastName;}

    friend std::istream& operator>>(std::istream& in, Student& s);

    // konstruktory
    Student();
    Student(const unsigned int &ID, const std::string &name, const std::string &lastName);
};

std::ostream& operator<<(std::ostream& out, const Student& s);

class StudentDatabase {
    // wektor z obiektami klasy Student
    std::vector<Student> studentDatabase;

public:
    // metody do obsługi, tj.:

    // dodawanie nowego studenta (nr indeksu, imię oraz nazwisko),
    void addStudent();

    // drukowanie całej listy studentów,
    void print();

    // wyszukiwanie studentów po numerze indeksu,
    void find();

    // usuwanie wybranych studentów (po numerze indeksu),
    void removeStudent();

    // zapis całej bazy do pliku,
    void save();

    // odczyt całej bazy z pliku,
    void load();

    // czyszczenie całej bazy studentów.
    void clear();
};

#endif //JIPP_02_STUDENTDATABASE_HPP
