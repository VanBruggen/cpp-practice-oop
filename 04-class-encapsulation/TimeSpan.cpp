#include "TimeSpan.hpp"
#include <cmath>
#include <iomanip>

TimeSpan::TimeSpan() :
      m_hours(0),
      m_minutes(0),
      m_seconds(0) {}

TimeSpan::TimeSpan(unsigned int seconds) {

    m_hours = seconds / 3600;
    seconds = seconds - (m_hours * 3600);

    m_minutes = seconds / 60;
    seconds = seconds - (m_minutes * 60);

    m_seconds = seconds;
}

void TimeSpan::print() {

    std::cout << "\n\n" << m_hours << ":" << std::setfill('0') << std::setw(2) << m_minutes << ":" << std::setw(2) << m_seconds << std::endl;
}