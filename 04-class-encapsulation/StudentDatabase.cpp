#include "StudentDatabase.hpp"

void AbyKontynuowac() {

    std::cout << "\n\nAby kontynuowac, wprowadz \" \' \" (apostrof).\n\n";

    char c;

    do {
        std::cout << "> ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        c = char(std::cin.get());
    } while (c != '\'');
}

Student::Student() :
    m_ID(0),
    m_name("noName"),
    m_lastName("noLastName") {std::cout << "\n\nWywolano konstruktor domyslny klasy Student.";}

Student::Student(const unsigned int &ID, const std::string &name, const std::string &lastName) :
    m_ID(ID),
    m_name(name),
    m_lastName(lastName) {std::cout << "\n\nWywowalno konstruktor klasy Student.";}

std::ostream& operator<<(std::ostream& out, const Student& s) {

    out << s.getID() << "\t" << s.getName() << "\t" << s.getLastName();

    return out;
}

std::istream& operator>>(std::istream& in, Student& s) {

    in >> s.m_ID >> s.m_name >> s.m_lastName;

    return in;
}

void StudentDatabase::addStudent() {

    std::cout << "\n\nPodaj numer indeksu, imie i nazwisko (bez polskich znakow), oddzielone spacjami:\n"
                 "> ";
    unsigned int ID;
    std::string name;
    std::string lastName;

    std::cin >> ID;
    std::cin >> name;
    std::cin >> lastName;

    studentDatabase.emplace_back(ID, name, lastName);

    std::cout << "\n\nDodano nowego studenta.";
    AbyKontynuowac();
}

void StudentDatabase::print() {

    std::cout << "\n\nIndeks\tImie\tNazwisko\n";

    for (const Student& s : studentDatabase) {

        std::cout << s << '\n';
    }

    AbyKontynuowac();
}

void StudentDatabase::find() {

    std::cout << "\n\nPodaj numer indeksu studenta, ktorego chcesz znalezc:\n"
                 "> ";

    bool foundStudent = false;

    unsigned int ID;
    std::cin >> ID;

    for (const Student& s : studentDatabase) {

        if (s.getID() == ID) {
            foundStudent = true;
            std::cout << "\n\nZnaleziono studenta o podanym numerze indeksu:\n"
                      << s;
            break;
        }
    }

    if (!foundStudent) std::cout << "\n\nNie znaleziono studenta o podanym numerze indeksu.";

    AbyKontynuowac();
}

void StudentDatabase::removeStudent() {

    std::cout << "\n\nPodaj numer indeksu studenta, ktorego chcesz usunac:\n"
                 "> ";

    bool foundStudent = false;

    unsigned int ID;
    std::cin >> ID;

    for (unsigned short i=0; i < studentDatabase.size(); ++i) {

        if (studentDatabase.at(i).getID() == ID) {
            foundStudent = true;
            studentDatabase.erase(studentDatabase.begin() + i);
            std::cout << "\n\nUsunieto studenta o podanym numerze indeksu.";
            break;
        }
    }

    if (!foundStudent) std::cout << "\n\nNie znaleziono studenta o podanym numerze indeksu.";

    AbyKontynuowac();
}

void StudentDatabase::save() {

    std::cout << "\n\nPodaj nazwe pliku (z rozszerzeniem \".txt\", bez spacji i polskich znakow), do ktorego chcesz zapisac zawartosc bazy"
                 "\n[UWAGA! Jesli wskazany plik juz istnieje, jego zawartosc zostanie nadpisana]:\n"
                 "> ";

    std::string nazwaPliku;
    std::cin >> nazwaPliku;

    std::ofstream fout(nazwaPliku, std::ios::trunc);

    if (fout.is_open() && fout.good()) {

        for (const Student& s : studentDatabase) {

            fout << s << '\n';
        }

        std:: cout << "\n\nZawartosc bazy zapisano w pliku \"" << nazwaPliku << "\".";

    } else {

        std::cout << "\n\nNie udalo sie otworzyc wskazanego pliku.";
    }

    fout.close();

    AbyKontynuowac();
}

void StudentDatabase::load() {

    std::cout << "\n\nPodaj nazwe pliku (z rozszerzeniem \".txt\", bez spacji i polskich znakow), z ktorego chcesz wczytac zawartosc bazy:\n"
                 "> ";

    std::string nazwaPliku;
    std::cin >> nazwaPliku;

    std::ifstream fin(nazwaPliku);

    if (fin.is_open() && fin.good()) {

        studentDatabase.clear();
        unsigned short counter=0;

        while (!fin.eof()) {

            studentDatabase.emplace_back(Student{});
            fin >> studentDatabase.at(counter);
            fin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            ++counter;

            // std::ifstream::traits_type::eof()
            // std::char_traits<char>::eof()

            if (fin.peek() == std::char_traits<char>::eof()) fin.ignore();
        }

        std:: cout << "\n\nWczytano do bazy zawartosc pliku \"" << nazwaPliku << "\".";

    } else {

        std::cout << "\n\nNie udalo sie otworzyc wskazanego pliku.";
    }

    fin.close();

    AbyKontynuowac();
}

void StudentDatabase::clear() {

    std::cout << "\n\nCzy jestes pewien? [Tak = \"T\"/\"t\", Nie = dowolny inny znak]\n"
                 "> ";

    char choice;
    std::cin >> choice;

    if (choice == 'T' || choice == 't') {

        studentDatabase.clear();
        std::cout << "\n\nWyczyszczono baze studentow.";

        AbyKontynuowac();
    }
}
