#include "Shapes.hpp"

void fill_sp(ShapeContainer& sp) {

    Shape* rec01 = new Rectangle("Rectangle_01", 3, 4);
    Shape* sq01 = new Square("Square_01", 3);
    Shape* cir01 = new Circle("Circle_01", 5);
    Shape* elipse01 = new Elipse("Elipse_01", 4, 3);

    sp.add(rec01);
    sp.add(sq01);
    sp.add(cir01);
    sp.add(elipse01);
}

int main() {

    ShapeContainer sp;

    fill_sp(sp);

    sp.displayAll();

    std::cout << "\n\nPowierzchnia wszystkich figur: " << sp.totalArea();

    std::cout << "\n\nWszystkie figury z polem wiekszym niz 30:"
                 "\nNazwa\tPole\tObwod";

    std::vector<Shape*> greaterThan = sp.getGreaterThan(30);

    for (const auto& gr : greaterThan) {

        std::cout << "\n" << gr->getName() << "\t" << gr->area() << "\t" << gr->perimeter();
    }

    AbyKontynuowac();

    return 0;
}