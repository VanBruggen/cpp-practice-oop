#include "Shapes.hpp"

const double Pi = 3.1415;

void AbyKontynuowac() {

    std::cout << "\n\nAby kontynuowac, wprowadz \" \' \" (apostrof).\n\n";

    char c;

    do {
        std::cout << "> ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        c = char(std::cin.get());
    } while (c != '\'');
}

// ======================================================================================================== Zadanie 1:

//  ======================================================================================   Shape
Shape::Shape(std::string name) :
    m_name(std::move(name)) {

        std::cout << "\n\nWywolano konstruktor klasy Shape.";
        std::cout << "\nNazwa ksztaltu: " << getName();
}

Shape::~Shape() {

    std::cout << "\n\nWywolano destruktor klasy Shape.";
    std::cout << "\nNazwa ksztaltu: " << getName();
}

//  =====================================================================================   Rectangle
Rectangle::Rectangle(std::string name, const double& width, const double& height) :
        Shape(std::move(name)),
        m_width(width),
        m_height(height) {

    std::cout << "\n\nWywolano konstruktor klasy Rectangle.";
    std::cout << "\nNazwa prostokata: " << getName();
}

Rectangle::~Rectangle() {

    std::cout << "\n\nWywolano destruktor klasy Rectangle.";
    std::cout << "\nNazwa prostokata: " << getName();
}

double Rectangle::area() const {

    return m_height * m_width;
}

double Rectangle::perimeter() const {

    return (2 * m_width) + (2 * m_height);
}

//  ======================================================================================   Square
Square::Square(std::string name, const double& width) :
        Rectangle(std::move(name), width, width) {

    std::cout << "\n\nWywolano konstruktor klasy Square.";
    std::cout << "\nNazwa kwadratu: " << getName();
}

Square::~Square() {

    std::cout << "\n\nWywolano destruktor klasy Square.";
    std::cout << "\nNazwa kwadratu: " << getName();
}

//  ======================================================================================   Circle
Circle::Circle(std::string name, const double& radius) :
    Shape(std::move(name)),
    m_radius(radius) {
        std::cout << "\n\nWywolano konstruktor klasy Circle.";
}

Circle::~Circle() {
    std::cout << "\n\nWywolano destruktor klasy Circle.";
}

double Circle::area() const {
    return Pi * m_radius * m_radius;
}

double Circle::perimeter() const {
    
    return 2 * Pi * m_radius; 
}

//  =======================================================================================   Elipse
Elipse::Elipse(std::string name, const double& majorAxis, const double& minorAxis) :
    Shape(std::move(name)),
    m_majorAxis(majorAxis),
    m_minorAxis(minorAxis) {

        std::cout << "\n\nWywolano konstruktor klasy Elipse.";
}

Elipse::~Elipse() {std::cout << "\n\nWywolano destruktor klasy Elipse.";}

double Elipse::area() const {

    return Pi * m_majorAxis * m_minorAxis;
}

double Elipse::perimeter() const {

    return Pi * (3.0/2 * (m_majorAxis + m_minorAxis) - sqrt(m_majorAxis * m_minorAxis));
}

//  =======================================================================================   ShapeContainer
ShapeContainer::~ShapeContainer() {

    for (auto& sh : shapeList) {

        delete sh;
    }
}

void ShapeContainer::add(Shape* pShape) {

    shapeList.push_back(pShape);

    std::cout << "\n\nDodano nowy ksztalt do bazy.";
}

void ShapeContainer::displayAll() const {

    std::cout << "\n\nNazwa\tPole\tObwod\n";

    for (const auto& sh : shapeList) {

        std::cout << sh->getName() << "\t" << sh->area() << "\t" << sh->perimeter() << "\n";
    }
}

double ShapeContainer::totalArea() const {

    double totalArea = 0.0;

    for (const auto& sh : shapeList) {

        totalArea += sh->area();
    }

    return totalArea;
}

std::vector<Shape*> ShapeContainer::getGreaterThan(const double& area) {

    std::vector<Shape*> result;

    for (const auto& sh : shapeList) {

        if (sh->area() > area) result.push_back(sh);
    }

    return result;
}